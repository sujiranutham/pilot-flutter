import 'dart:convert';
import 'dart:developer';
import 'package:http/http.dart' as http;
import 'package:politflutter/service/entity/user.model.dart';

class Api {
  static const baseURL = 'http://localhost:3000/api/';

  var client = new http.Client();

  Future<UserModel> getUserProfile(int userId) async {
    var response = await client.get('$baseURL/v1.0/user/getAll');
    log(response.body.toString());
    return UserModel.fromJson(json.decode(response.body));
  }
}
