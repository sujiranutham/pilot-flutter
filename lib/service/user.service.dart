import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:politflutter/service/entity/user.model.dart';

class UserService {
  static const baseURL = 'http://192.168.1.102:3000/api';

  static Future<List<UserModel>> getAll() async {
    String full = '$baseURL/v1.0/user/getAll';
    var response = await http.get(full);
    List res = json.decode(response.body);
    List<UserModel> users = res.map((m) => new UserModel.fromJson(m)).toList();
    return users;
  }
}
