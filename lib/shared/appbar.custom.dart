import 'package:flutter/material.dart';

class AppBarCustom extends StatelessWidget with PreferredSizeWidget {
  @override
  final Size preferredSize;

  final String title;
  final IconData icon;
  // AppBarCustom(String title,Icon)
  AppBarCustom(
    this.title,
    this.icon, {
    Key key,
  })  : preferredSize = Size.fromHeight(50.0),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Center(
          child: Row(
        children: <Widget>[
          Icon(icon),
          Text('  ' + title),
        ],
      )),
      automaticallyImplyLeading: true,
    );
  }
}
