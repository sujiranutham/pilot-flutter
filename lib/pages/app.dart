import 'package:flutter/material.dart';
import 'package:politflutter/pages/about/about.dart';
import 'package:politflutter/pages/home/home.dart';
import 'package:politflutter/pages/user/user.dart';
import 'package:politflutter/shared/appbar.custom.dart';

class AppPage extends StatefulWidget {
  @override
  _AppPage createState() => _AppPage();
}

class _AppPage extends State<AppPage> {
  String title = "Home Page";
  IconData icon = Icons.home;
  int _currentIndex = 0;
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarCustom(title, icon),
      body: _changePageFromIndex(_currentIndex),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        onTap: (index) {
          setState(() {
            _currentIndex = index;
          });
        },
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text(
              'Home',
              style: new TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            backgroundColor: Colors.blue,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person_pin),
            title: Text(
              'Meaww~',
              style: new TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            backgroundColor: Colors.blue,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.perm_device_information),
            title: Text('About'),
            backgroundColor: Colors.blue,
          )
        ],
      ),
    );
  }

  // void _setIconNavigator(String _title, IconData _icon) {
  //   setState(() {
  //     title = _title;
  //     icon = _icon;
  //   });
  // }

  Widget _changePageFromIndex(int index) {
    switch (index) {
      case 0:
        return HomePage();
      case 1:
        return UserPage();
      case 2:
        // _setIconNavigator("About page", Icons.home);
        return AboutPage();
      default:
        return HomePage();
    }
  }
}
