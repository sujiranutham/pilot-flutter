import 'package:flutter/material.dart';
// import 'package:politflutter/pages/about/about.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    String src =
        "https://sites.google.com/site/funnycatmeawww/_/rsrc/1422326075261/home/6997052-funny-cat.jpg?height=675&width=1200";
    return Scaffold(
        body: Container(
      constraints: new BoxConstraints.expand(
        height: MediaQuery.of(context).size.height,
      ),
      alignment: Alignment.topRight,
      padding: new EdgeInsets.only(right: 0.0, top: 90.0),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: NetworkImage(src),
          fit: BoxFit.cover,
        ),
      ),
      child: Container(
          padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
          child: new Text(
              'Do you believe in destiny ? or did I walked past you again.. huh ?',
              style: new TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.white,
                fontSize: 24.0,
              ))),
    ));
  }
}
