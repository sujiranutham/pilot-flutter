import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:politflutter/pages/user/user.info.dart';
import 'package:politflutter/service/entity/user.model.dart';
import 'package:politflutter/service/user.service.dart';
import 'package:toast/toast.dart';

class UserPage extends StatefulWidget {
  @override
  _UserPage createState() => _UserPage();
}

class _UserPage extends State<UserPage> {
  String src =
      "https://sites.google.com/site/funnycatmeawww/_/rsrc/1422326075261/home/6997052-funny-cat.jpg?height=675&width=1200";

  @override
  void initState() {
    super.initState();
  }

  void _openModal(UserModel user) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => UserInfo(user)),
    );
  }

  Widget _generateSlideLeft(UserModel user) {
    return Slidable(
      actionPane: SlidableDrawerActionPane(),
      actionExtentRatio: 0.25,
      child: _generateListCat(user),
      secondaryActions: <Widget>[
        Padding(
          padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
          child: IconSlideAction(
            caption: 'Edit',
            color: Colors.blue[600],
            icon: Icons.mode_edit,
            onTap: () {
              _openModal(user);
              print('more');
            },
          ),
        ),
        Padding(
          padding: EdgeInsets.fromLTRB(0, 5, 2, 5),
          child: IconSlideAction(
            caption: 'Delete',
            color: Colors.red,
            icon: Icons.delete,
            onTap: () {
              print('Delete');
            },
          ),
        ),
      ],
    );
  }

  Widget _generateListCat(UserModel user) {
    return new Container(
      child: Card(
          child: Container(
        width: MediaQuery.of(context).size.width,
        height: 120,
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 5,
              child: Container(
                height: 120,
                child: Image.network(src, fit: BoxFit.cover),
              ),
            ),
            Expanded(
              flex: 7,
              child: Container(
                padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Flexible(
                      child: Container(
                        padding: EdgeInsets.only(top: 10),
                        child: Text(
                          user.firstname + " " + user.lastname,
                          textAlign: TextAlign.left,
                          overflow: TextOverflow.ellipsis,
                          style: new TextStyle(
                            fontSize: 22,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                    Flexible(
                      child: Container(
                        padding: EdgeInsets.only(top: 5),
                        child: Text(
                          "Some quick example text to build on the card title and make up the bulk of the card's content.",
                          textAlign: TextAlign.left,
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                          style: new TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w500,
                              color: Colors.grey[700]),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      )),
    );
  }

  Widget _showUserList() {
    return Container(
      padding: EdgeInsets.only(top: 5),
      child: FutureBuilder<List<UserModel>>(
        future: UserService.getAll(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            List<UserModel> user = snapshot.data;
            return Container(
              child: ListView.builder(
                physics: BouncingScrollPhysics(),
                scrollDirection: Axis.vertical,
                itemCount: user.length,
                itemBuilder: (context, index) {
                  return _generateSlideLeft(user[index]);
                  // return _generateListCat(user[index]);
                },
              ),
            );
          } else if (snapshot.hasError) {
            return Text("${snapshot.error}");
          }
          // By default, show a loading spinner.
          return Center(child: CircularProgressIndicator());
        },
      ),
    );
  }

  void _showToast() {
    Toast.show("Toast plugin app", context, duration: 1, gravity: Toast.BOTTOM);
  }

  Widget build(BuildContext context) {
    print("asdasdasd");
    return Scaffold(
      body: RefreshIndicator(
        child: _showUserList(),
        onRefresh: () async {
          _showToast();
          return _showUserList();
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => {_openModal(null)},
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}
