import 'package:flutter/material.dart';
import 'package:politflutter/service/entity/user.model.dart';
import 'package:politflutter/service/user.service.dart';
import 'package:politflutter/shared/appbar.custom.dart';
import 'package:politflutter/shared/dialog.custom.dart';

class UserInfo extends StatefulWidget {
  final UserModel userModelData;
  UserInfo(this.userModelData, {Key key});
  @override
  _UserInfo createState() => _UserInfo(userModelData);
}

class _UserInfo extends State<UserInfo> {
  final UserModel userModelData;
  _UserInfo(this.userModelData);

  final _formKey = GlobalKey<FormState>();
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  UserModel data;

  @override
  void initState() {
    super.initState();
  }

  Future<Widget> _save(BuildContext context) async {
    DialogCustom.showLoadingDialog(context, _keyLoader);
    await UserService.getAll();
    await Future.delayed(
      const Duration(seconds: 5),
      () => {debugPrint("asdasdas")},
    );
    Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
    // return Text("asdasdasd");
    // Navigator.pop(context);
  }

  Widget build(BuildContext context) {
    print("asdasdasd");
    setState(() {
      data = (userModelData == null) ? new UserModel() : userModelData;
    });

    print(userModelData);
    return Scaffold(
      appBar: AppBarCustom("User Information", Icons.supervised_user_circle),
      body: Center(
        child: GestureDetector(
          child: Container(
            padding: EdgeInsets.fromLTRB(15, 0, 15, 15),
            child: Card(
              child: Container(
                padding: EdgeInsets.fromLTRB(20, 30, 20, 30),
                child: Form(
                  key: _formKey,
                  child: Wrap(
                    children: <Widget>[
                      Container(
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                            Container(
                              child: Text(
                                "Firstname",
                                style: new TextStyle(
                                    fontSize: 18,
                                    color: Colors.grey[500],
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(top: 5),
                              color: Colors.white,
                              child: TextFormField(
                                initialValue: data.firstname,
                                decoration: new InputDecoration(
                                  hintText: "Please input firstname",
                                ),
                                style: new TextStyle(
                                  fontSize: 20,
                                ),
                              ),
                            )
                          ])),
                      Container(
                        padding: EdgeInsets.only(top: 40),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                child: Text(
                                  "Lastname",
                                  style: new TextStyle(
                                      fontSize: 18,
                                      color: Colors.grey[500],
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(top: 5),
                                color: Colors.white,
                                child: TextFormField(
                                  initialValue: data.lastname,
                                  decoration: new InputDecoration(
                                    hintText: "Please input lastname",
                                  ),
                                  style: new TextStyle(
                                    fontSize: 20,
                                  ),
                                ),
                              )
                            ]),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 40),
                        width: MediaQuery.of(context).size.width,
                        child: new RaisedButton(
                          padding: EdgeInsets.fromLTRB(0, 10, 20, 10),
                          color: Colors.teal[900],
                          onPressed: () {
                            _save(context);
                          },
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              new Icon(
                                Icons.save,
                                color: Colors.white,
                                size: 30,
                              ),
                              new Text(
                                ' Save',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 24,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 20),
                        width: MediaQuery.of(context).size.width,
                        child: new RaisedButton(
                          padding: EdgeInsets.fromLTRB(0, 10, 20, 10),
                          color: Colors.grey[600],
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              new Icon(
                                Icons.close,
                                color: Colors.white,
                                size: 30,
                              ),
                              new Text(
                                ' Cancel',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 24,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
